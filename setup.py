# -*- coding: utf-8 -*-
"""`sphinx_ewars_theme` lives on `Gitlab`_.

.. _gitlab: https://gitlab.com/ewars/sphinx-ewars-theme

"""
from setuptools import setup


setup(
    name='sphinx_ewars_theme',
    url='https://gitlab.com/ewars/sphinx-ewars-theme',
    license='BSD',
    author='Jeff Uren',
    author_email='jduren@protonmail.com',
    description='Apple Developer Connection theme for Sphinx, 2019 version.',
    long_description=open('README.rst').read(),
    zip_safe=False,
    keywords='sphinx theme',
    packages=['sphinx_ewars_theme'],
    package_data={
        'sphinx_ewars_theme': [
            'theme.conf',
            '*.html',
            'static/css/*.css',
            'static/js/*.js',
            'static/img/*.*'
        ]
    },
    include_package_data=True,
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: BSD License',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Framework :: Sphinx',
        'Framework :: Sphinx :: Theme',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Operating System :: OS Independent',
        'Topic :: Documentation',
        'Topic :: Software Development :: Documentation',
    ],
    entry_points = {
        'sphinx.html_themes': [
            'sphinx_ewars_theme = sphinx_ewars_theme',
        ]
    },
)
